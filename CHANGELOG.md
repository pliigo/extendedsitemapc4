# Changelog extendedSitemap [sd_extendedSitemap]  
Download der Erweiterung über composer innerhalb Contao oder über die [Gitlab-Seite](https://gitlab.com/paddy0174/extendedSitemap) der Erweiterung.  

## Version  

### 0.0.5  
* (A) Add composer.json  

### 0.0.4  
* (C) Cleaning up the code for release  

### 0.0.3  
* (A) Sitemap-Index
* (A) Show articles with teaser seperately
* (F) Forgot to add the correct xhtml schema (language-relations)  
* (F) Fix incorrect root language for related pages  

### 0.0.2  
* (A) Content element type: gallery  
* (A) Update lastmod from page, article or content element  
* (A) Ping Google/Bing  

### 0.0.1  
* (A) Initial release   

Zeichenerklärung: (A) add - Hinzugefügt / (F) fix - Fehlerbehebung / (C) change - Änderung  
