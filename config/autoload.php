<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2017 Leo Feyer
 *
 * @package sd_extendedSitemap
 *
 * @copyright SmithData / Patrick Smith 2017
 *
 * @license GPLv3+
 *
 * @author Patrick Smith <https://smithdata.de>
 * 
 * Based on googleSitemap from Andreas Schempp <https://terminal42.ch>
 */
 
 
 /**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
    'smithdata\extendedSitemap',
));


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Overwrite core class
	'smithdata\extendedSitemap\Automator' => 'system/modules/sd_extendedSitemap/classes/extendedSitemapAutomator.php',
	'smithdata\extendedSitemap\ExtendedSitemap' => 'system/modules/sd_extendedSitemap/classes/extendedSitemap.php',
	'smithdata\extendedSitemap\ExtendedSitemapLanguageRelations' => 'system/modules/sd_extendedSitemap/classes/extendedSitemapLanguageRelations.php'
));

 ?>