# extendedSitemap [sd_extendedSitemap] 

#### Modul für Contao CMS zur Erweiterung / Verbesserung der im Standard erstellten Sitemap.xml  

#### Version: 0.0.5  

## Systemvoraussetzungen  
* Contao CMS Version 3.5.x  
	* Die Systemvoraussetzungen richten sich nach der jeweils verwendeten Contao Version. Prüfen Sie daher die Systemvoraussetzungen auf der [Contao Homepage](https://www.contao.org).  
	Diese Erweiterung wird jeweils mit der aktuellesten Contao Version getestet und freigegeben (aktuell: 3.5.25 LTS)
* Erforderliche Contao-Module:  
    * n/a  
* Empfohlene Contao-Module:
	* [Hofff/contao-language-relations](https://github.com/hofff/contao-language-relations)  

## Erklärung  
Mit Hilfe dieser Erweiterung wird die Erstellung der Sitemap.xml-Datei für Suchmaschinen erweitert.  
* Seiten:
	* URL: die URL zur Seite (Contao Standard Verhalten)  
	* Lastmod: Datum der letzten Modifikation der Seite (automatisch anhand der letzten Änderung eines ContentElements oder Artikels oder manuelle Eingabe möglich)  
	* Changefreq: Änderungshäufigkeit der Seite
	* Priority: Priorität der Seite
	* Sprachreferenz (nur mit installierter Erweiterung [Hofff/contao-language-relations](https://github.com/hofff/contao-language-relations)), hierbei werden alle Sprachen untereinander verlinkt.  
* Inhaltselemente (einzeln auswählbar ob Darstellung in Sitemap gewünscht oder nicht):
	* Bild (als eigenes Inhaltselement oder als hinzugefügtes Bild bei Text)  
		* URL des Bildes  
		* Überschrift des Bildes  
		* Bildunterschrift  
		* GeoLocation des Bildes  
		* Lizenzangaben zum Bild  
	* Download  
		* URL der Datei  
	* Downloads  
		* URL der Datei  
	* Galerie  
		* URL der einzelnen Bilder  
		* Überschrift der Galerie  
		* GeoLocation der Galerie  
		* Lizenzangaben zur Galerie  
	* *Video (kommende Version)*    
* Allgemein:  
	* Ping an Google und Bing senden, sobald eine neue Sitemap erstellt wurde  
	* *Automatisierte Erstellung einer Sitemap-Index-Datei (kommende Version)*  

Weitere Informationen erhalten Sie im [Wiki der Erweiterung](https://gitlab.com/paddy0174/extendedSitemap/wikis/home).  