<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2017 Leo Feyer
 *
 * @package sd_extendedSitemap
 *
 * @copyright SmithData / Patrick Smith 2017
 *
 * @license GPLv3+
 *
 * @author Patrick Smith <https://smithdata.de>
 * 
 * Based on googleSitemap from Andreas Schempp <https://terminal42.ch>
 */


/**
 * Table tl_settings
 */
$GLOBALS['TL_DCA']['tl_settings']['palettes']['default'] = str_replace('disableIpCheck;', 'disableIpCheck;{sd_extendedSitemap_legend:hide},activateSitemapIndex;', $GLOBALS['TL_DCA']['tl_settings']['palettes']['default']);

$GLOBALS['TL_DCA']['tl_settings']['palettes']['__selector__'][] = 'activateSitemapIndex';
$GLOBALS['TL_DCA']['tl_settings']['subpalettes']['activateSitemapIndex'] = 'sitemapIndexName,sitemapPingGoogleBing';

$GLOBALS['TL_DCA']['tl_settings']['fields']['activateSitemapIndex'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_settings']['sd_extendedSitemap_activateSitemapIndex'],
	'inputType'               => 'checkbox',
	'eval'                    => array('submitOnChange'=>true)
);

$GLOBALS['TL_DCA']['tl_settings']['fields']['sitemapIndexName'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_settings']['sd_extendedSitemap_sitemapIndexName'],
	'exclude'                 => true,
	'inputType'               => 'text',
	'eval'                    => array('mandatory'=>true, 'rgxp'=>'alnum', 'decodeEntities'=>true, 'maxlength'=>32, 'tl_class'=>'w50')
);

$GLOBALS['TL_DCA']['tl_settings']['fields']['sitemapPingGoogleBing'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_settings']['sd_extendedSitemap_sitemapPingGoogleBing'],
	'exclude'                 => true,
	'inputType'               => 'checkbox',
	'eval'                    => array('tl_class'=>'w50 m12')
);