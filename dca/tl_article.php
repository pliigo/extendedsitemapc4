<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2017 Leo Feyer
 *
 * @package sd_extendedSitemap
 *
 * @copyright SmithData / Patrick Smith 2017
 *
 * @license GPLv3+
 *
 * @author Patrick Smith <https://smithdata.de>
 * 
 * Based on googleSitemap from Andreas Schempp <https://terminal42.ch>
 */


/**
 * Table tl_article
 */
$GLOBALS['TL_DCA']['tl_article']['config']['onsubmit_callback'][] = array('\smithdata\extendedSitemap\ExtendedSitemap', 'updateSitemapLastmod');

?>