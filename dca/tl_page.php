<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2017 Leo Feyer
 *
 * @package sd_extendedSitemap
 *
 * @copyright SmithData / Patrick Smith 2017
 *
 * @license GPLv3+
 *
 * @author Patrick Smith <https://smithdata.de>
 * 
 * Based on googleSitemap from Andreas Schempp <https://terminal42.ch>
 */


/**
 * Table tl_page
 */
$GLOBALS['TL_DCA']['tl_page']['subpalettes']['createSitemap'] = 'sitemapName,inSitemapIndex';

$GLOBALS['TL_DCA']['tl_page']['palettes']['regular'] = str_replace(',sitemap,', ',', $GLOBALS['TL_DCA']['tl_page']['palettes']['regular']);
$GLOBALS['TL_DCA']['tl_page']['palettes']['regular'] = str_replace(';{expert_legend', ';{sitemap_legend},sitemap,sitemapPriority,sitemapChangefreq,sitemapLastmodAutomaticOff;{expert_legend', $GLOBALS['TL_DCA']['tl_page']['palettes']['regular']);

$GLOBALS['TL_DCA']['tl_page']['palettes']['__selector__'][] = 'sitemapLastmodAutomaticOff';
$GLOBALS['TL_DCA']['tl_page']['subpalettes']['sitemapLastmodAutomaticOff'] = 'sitemapLastmodDate';

$GLOBALS['TL_DCA']['tl_page']['fields']['sitemap']['eval'] = array_replace($GLOBALS['TL_DCA']['tl_page']['fields']['sitemap']['eval'], array('tl_class' => 'w50 clr'));
$GLOBALS['TL_DCA']['tl_page']['fields']['hide']['eval'] = array_replace($GLOBALS['TL_DCA']['tl_page']['fields']['cssClass']['eval'], array('tl_class' => 'w50 clr'));

$GLOBALS['TL_DCA']['tl_page']['fields']['inSitemapIndex'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_page']['sd_extendedSitemap_inSitemapIndex'],
	'exclude'                 => true,
	'inputType'               => 'checkbox',
	'eval'                    => array('tl_class'=>'w50 m12'),
	'sql'                     => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_page']['fields']['sitemapPriority'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_page']['sd_extendedSitemap_sitemapPriority'],
	'exclude'                 => true,
	'inputType'               => 'select',
	'options'				  => array('1'=>'0.1','2'=>'0.2','3'=>'0.3','4'=>'0.4','5'=>'0.5','6'=>'0.6','7'=>'0.7','8'=>'0.8','9'=>'0.9', '0'=>'1.0'),
	'eval'                    => array('tl_class'=>'w50 clr wizard'),
	'sql'                     => "int(1) NOT NULL default '5'"
);

$GLOBALS['TL_DCA']['tl_page']['fields']['sitemapChangefreq'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_page']['sd_extendedSitemap_sitemapChangefreq'],
	'exclude'                 => true,
	'inputType'               => 'select',
	'options'				  => array('always','hourly','daily','weekly','monthly','yearly','never'),
	'eval'                    => array('tl_class'=>'w50'),
	'sql'                     => "varchar(10) NOT NULL default 'monthly'"
);

$GLOBALS['TL_DCA']['tl_page']['fields']['sitemapLastmodAutomaticOff'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_page']['sd_extendedSitemap_sitemapLastmodAutomaticOff'],
	'exclude'                 => true,
	'inputType'               => 'checkbox',
	'eval'                    => array('tl_class'=>'w50 clr','submitOnChange'=>true),
	'sql'                     => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_page']['fields']['sitemapLastmodDate'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_page']['sd_extendedSitemap_sitemapLastmodDate'],
	'exclude'                 => true,
	'inputType'               => 'text',
	'eval'                    => array('rgxp'=>'date', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
	'sql'                     => "varchar(11) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_page']['fields']['sitemapLastPing'] = array
(
	'sql'                     => "int(10) unsigned NOT NULL default '0'"
);

$GLOBALS['TL_DCA']['tl_page']['config']['onsubmit_callback'][] = array('\smithdata\extendedSitemap\ExtendedSitemap', 'updateSitemapLastmod');