<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2017 Leo Feyer
 *
 * @package sd_extendedSitemap
 *
 * @copyright SmithData / Patrick Smith 2017
 *
 * @license GPLv3+
 *
 * @author Patrick Smith <https://smithdata.de>
 * 
 * Based on googleSitemap from Andreas Schempp <https://terminal42.ch>
 */


/**
 * Table tl_content
 */
$GLOBALS['TL_DCA']['tl_content']['palettes']['image'] = str_replace('{expert_legend', '{sd_extendedSitemap_legend},addToSitemap,sitemapImageLocation,sitemapImageLicense;{expert_legend', $GLOBALS['TL_DCA']['tl_content']['palettes']['image']);
$GLOBALS['TL_DCA']['tl_content']['palettes']['download'] = str_replace('guests', 'guests,addToSitemap', $GLOBALS['TL_DCA']['tl_content']['palettes']['download']);
$GLOBALS['TL_DCA']['tl_content']['palettes']['downloads'] = str_replace('guests', 'guests,addToSitemap', $GLOBALS['TL_DCA']['tl_content']['palettes']['downloads']);
$GLOBALS['TL_DCA']['tl_content']['palettes']['gallery'] = str_replace('guests', 'guests,addToSitemap,sitemapImageLocation,sitemapImageLicense', $GLOBALS['TL_DCA']['tl_content']['palettes']['gallery']);
//$GLOBALS['TL_DCA']['tl_content']['palettes']['player'] = str_replace('guests', 'guests,addToSitemap', $GLOBALS['TL_DCA']['tl_content']['palettes']['player']);

$GLOBALS['TL_DCA']['tl_content']['subpalettes']['addImage'] = str_replace('floating', 'floating,addToSitemap,sitemapImageLocation,sitemapImageLicense', $GLOBALS['TL_DCA']['tl_content']['subpalettes']['addImage']);

$GLOBALS['TL_DCA']['tl_content']['fields']['addToSitemap'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_content']['sd_extendedSitemap_addToSitemap'],
	'exclude'                 => true,
	'inputType'               => 'checkbox',
	'eval'                    => array('tl_class'=>'w50'),
	'sql'                     => "char(1) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_content']['fields']['sitemapImageLocation'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_content']['sd_extendedSitemap_sitemapImageLocation'],
	'exclude'                 => true,
	'inputType'               => 'text',
	'eval'                    => array('tl_class'=>'w50 clr'),
	'sql'                     => "varchar(255) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_content']['fields']['sitemapImageLicense'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_content']['sd_extendedSitemap_sitemapImageLicense'],
	'exclude'                 => true,
	'inputType'               => 'select',
	'options_callback'		  => array('ExtendedSitemap', 'getImageLicenses'),
	'eval'                    => array('tl_class'=>'w50','includeBlankOption'=>true,'chosen'=>true,'rgxp'=>'url'),
	'sql'                     => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_content']['config']['onsubmit_callback'][] = array('\smithdata\extendedSitemap\ExtendedSitemap', 'updateSitemapLastmod');

?>