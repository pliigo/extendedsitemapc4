<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2017 Leo Feyer
 *
 * @package sd_extendedSitemap
 *
 * @copyright SmithData / Patrick Smith 2017
 *
 * @license GPLv3+
 *
 * @author Patrick Smith <https://smithdata.de>
 * 
 * Based on googleSitemap from Andreas Schempp <https://terminal42.ch>
 */


/**
 * tl_page
 */
$GLOBALS['TL_LANG']['tl_page']['sd_extendedSitemap_inSitemapIndex'] = array('In SitemapIndex', 'Diese Sitemap in die SitemapIndex-Datei aufnehmen.');
$GLOBALS['TL_LANG']['tl_page']['sd_extendedSitemap_sitemapPriority'] = array('Priorität dieser Seite', 'Eine gesonderte Priorität für diese Seite setzen (Standard: 0.5).');
$GLOBALS['TL_LANG']['tl_page']['sd_extendedSitemap_sitemapChangefreq'] = array('Änderungshäufigkeit', 'Vorraussichtliche Änderungshäufigkeit der Seite.');
$GLOBALS['TL_LANG']['tl_page']['sd_extendedSitemap_sitemapLastmodAutomaticOff'] = array('Letztes Änderungsdatum manuell hinzufügen', 'Sie können das letzte Änderungsdatum manuell setzen oder automatisch einfügen lassen.');
$GLOBALS['TL_LANG']['tl_page']['sd_extendedSitemap_sitemapLastmodDate'] = array('Letzte Änderung', 'Setzen Sie hier ein spezifisches Änderungsdatum.');