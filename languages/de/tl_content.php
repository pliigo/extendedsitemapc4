<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2017 Leo Feyer
 *
 * @package sd_extendedSitemap
 *
 * @copyright SmithData / Patrick Smith 2017
 *
 * @license GPLv3+
 *
 * @author Patrick Smith <https://smithdata.de>
 * 
 * Based on googleSitemap from Andreas Schempp <https://terminal42.ch>
 */


/**
 * tl_content
 */
$GLOBALS['TL_LANG']['tl_content']['sd_extendedSitemap_legend'] = 'Sitemap Einstellungen';
$GLOBALS['TL_LANG']['tl_content']['sd_extendedSitemap_addToSitemap'] = array('In Sitemap aufnehmen', 'Dieses Element explizit in die Sitemap aufnehmen.');
$GLOBALS['TL_LANG']['tl_content']['sd_extendedSitemap_sitemapImageLocation'] = array('Ortsangabe (Geolocation)', 'Tragen Sie hier den Ort der Aufnahme ein (z.B. "Marienplatz, München, Deutschland")');
$GLOBALS['TL_LANG']['tl_content']['sd_extendedSitemap_sitemapImageLicense'] = array('Bildlizenz', 'Wählen Sie hier eine Lizenz aus oder tragen Sie die <strong>URL</strong> die zu Ihrer Lizenz führt ein.');

?>