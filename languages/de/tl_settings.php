<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2017 Leo Feyer
 *
 * @package sd_extendedSitemap
 *
 * @copyright SmithData / Patrick Smith 2017
 *
 * @license GPLv3+
 *
 * @author Patrick Smith <https://smithdata.de>
 * 
 * Based on googleSitemap from Andreas Schempp <https://terminal42.ch>
 */


/**
 * tl_settings
 */
$GLOBALS['TL_LANG']['tl_settings']['sd_extendedSitemap_legend'] = 'Sitemap Einstellungen';
$GLOBALS['TL_LANG']['tl_settings']['sd_extendedSitemap_activateSitemapIndex'] = array('Eine SitemapIndex-Datei erstellen', 'Es wird eine SitemapIndex-Datei im Verzeichnis <i>share/</i> angelegt, in der alle einzelnen Sitemaps verzeichnet sind.');
$GLOBALS['TL_LANG']['tl_settings']['sd_extendedSitemap_sitemapIndexName'] = array('Name SitemapIndex-Datei', 'Tragen Sie hier den Namen der SitemapIndex-Datei ein.');
$GLOBALS['TL_LANG']['tl_settings']['sd_extendedSitemap_sitemapPingGoogleBing'] = array('Google und Bing pingen', 'Setzt bei Erstellung einer neuen Sitemap automatisch einen Ping an Google und Bing ab.');