<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2017 Leo Feyer
 *
 * @package sd_extendedSitemap
 *
 * @copyright SmithData / Patrick Smith 2017
 *
 * @license GPLv3+
 *
 * @author Patrick Smith <https://smithdata.de>
 * 
 * Based on googleSitemap from Andreas Schempp <https://terminal42.ch>
 */


/**
 * tl_content
 */
$GLOBALS['TL_LANG']['tl_content']['sd_extendedSitemap_legend'] = 'Sitemap settings';
$GLOBALS['TL_LANG']['tl_content']['sd_extendedSitemap_addToSitemap'] = array('List in sitemap', 'Show this element explicit in the sitemap file.');
$GLOBALS['TL_LANG']['tl_content']['sd_extendedSitemap_sitemapImageLocation'] = array('Geolocation', 'You can set a location for your element (e.g. "Marienplatz, Munich, Germany")');
$GLOBALS['TL_LANG']['tl_content']['sd_extendedSitemap_sitemapImageLicense'] = array('License', 'Choose your license for this image, to show in sitemap.');

?>