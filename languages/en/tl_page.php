<?php

/**
 * Contao Open Source CMS
 *
 * @license GPLv3
 *
 * @package sd_extendedSitemap
 *
 * @copyright SmithData / Patrick Smith 2017
 *
 * @author Patrick Smith <https://smithdata.de>
 */


/**
 * tl_page
 */
$GLOBALS['TL_LANG']['tl_page']['sd_extendedSitemap_inSitemapIndex'] = array('In SitemapIndex', 'List this sitemap in sitemapIndex');
$GLOBALS['TL_LANG']['tl_page']['sd_extendedSitemap_sitemapPriority'] = array('Page priority', 'Seta different priority for this page (standard: 0.5).');
$GLOBALS['TL_LANG']['tl_page']['sd_extendedSitemap_sitemapChangefreq'] = array('Change frequency', 'Estimated change frequency for the page.');
$GLOBALS['TL_LANG']['tl_page']['sd_extendedSitemap_sitemapLastmodAutomaticOff'] = array('Set last change date manually', 'You can set the last change date manually or leave it to the system, to take care for.');
$GLOBALS['TL_LANG']['tl_page']['sd_extendedSitemap_sitemapLastmodDate'] = array('Last change', 'Here you can set your own date.');