<?php

/**
 * Contao Open Source CMS
 *
 * @license GPLv3
 *
 * @package sd_extendedSitemap
 *
 * @copyright SmithData / Patrick Smith 2017
 *
 * @author Patrick Smith <https://smithdata.de>
 */


/**
 * tl_settings
 */
$GLOBALS['TL_LANG']['tl_settings']['sd_extendedSitemap_legend'] = 'Sitemap settings';
$GLOBALS['TL_LANG']['tl_settings']['sd_extendedSitemap_activateSitemapIndex'] = array('Use SitemapIndex file', 'A SitemapIndex file will be created in folder <i>share/</i>. All given sitemaps will be listed.');
$GLOBALS['TL_LANG']['tl_settings']['sd_extendedSitemap_sitemapIndexName'] = array('Name for SitemapIndex', 'The name for the SitemapIndex file.');
$GLOBALS['TL_LANG']['tl_settings']['sd_extendedSitemap_sitemapPingGoogleBing'] = array('Ping Google and Bing', 'Let Google and Bing automatically know about every change in your sitemap files.');