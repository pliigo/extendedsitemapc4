<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2017 Leo Feyer
 *
 * @package sd_extendedSitemap
 *
 * @copyright SmithData / Patrick Smith 2017
 *
 * @license GPLv3+
 *
 * @author Patrick Smith <https://smithdata.de>
 * 
 * Based on googleSitemap from Andreas Schempp <https://terminal42.ch>
 */

namespace smithdata\extendedSitemap;

use Hofff\Contao\LanguageRelations\LanguageRelations;

/**
 * Class extendedSitemapLanguageRelations
 */
class ExtendedSitemapLanguageRelations extends \Backend
{
	/**
	 * Get the language relations for the given page
	 *
	 * @param integer $id
	 *
	 * @return array
	 */
	public function findPageLanguageRelations($id)
	{
		$arrReturn = array();

		$arrLanguageRelations = LanguageRelations::getPagesRelatedTo($id);

		if ($arrLanguageRelations != null)
		{
			foreach ($arrLanguageRelations as $relatedPageId)
			{
				$objRelatedPage = \PageModel::findWithDetails($relatedPageId);

				if ($objRelatedPage != null) 
				{
					$arrReturn[] = array('language' => $objRelatedPage->rootLanguage, 'url' => $objRelatedPage->getAbsoluteUrl());
				}
			}
		}

		return $arrReturn;
	}
}