<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2017 Leo Feyer
 *
 * @package sd_extendedSitemap
 *
 * @copyright SmithData / Patrick Smith 2017
 *
 * @license GPLv3+
 *
 * @author Patrick Smith <https://smithdata.de>
 * 
 * Based on googleSitemap from Andreas Schempp <https://terminal42.ch>
 */

namespace smithdata\extendedSitemap;
use \Contao\Automator as ContaoAutomator;

/**
 * Class Automator
 */
class Automator extends ContaoAutomator
{
	/**
	 * Generate the Google XML sitemaps
	 *
	 * @param integer $intId The page ID
	 */
	public function generateSitemap($intId=0)
	{
		$this->purgeXmlFiles();
		\ExtendedSitemap::deleteSitemapNamesForNonRootPages();
		
		$objRoot = \ExtendedSitemap::findPublishedRootPages(array('column' => array('createSitemap=?','sitemapName!=?'), 'value' => array('1','')));

		if ($objRoot !== null)
		{
			// Create the XML file
			while ($objRoot->next())
			{
				if ($intId > 0 && $intId != $objRoot->id)
				{
					continue;
				}

				if ($objRoot->createSitemap == '1' && $objRoot->sitemapName != '')
				{
					$arrPages = \ExtendedSitemap::findSearchablePages($objRoot->id, '', true);

					// HOOK: take additional pages (contao standard)
					if (isset($GLOBALS['TL_HOOKS']['getSearchablePages']) && is_array($GLOBALS['TL_HOOKS']['getSearchablePages']))
					{
						foreach ($GLOBALS['TL_HOOKS']['getSearchablePages'] as $callback)
						{
							$this->import($callback[0]);
							$arrPagesNew = $this->{$callback[0]}->{$callback[1]}($arrPages, $objRoot->id, true, $objRoot->language);
						}
					}

					$arrPages = array_diff($arrPagesNew, $arrPages);
					
					$arrPagesWithOptions = \ExtendedSitemap::findSearchablePagesWithOptions($objRoot->id, true, $objRoot->language);
					
					// HOOK: take additional pages with options (new extension hook)
					if (isset($GLOBALS['TL_HOOKS']['findSearchablePagesWithOptions']) && is_array($GLOBALS['TL_HOOKS']['findSearchablePagesWithOptions']))
					{
						foreach ($GLOBALS['TL_HOOKS']['findSearchablePagesWithOptions'] as $callback)
						{
							$this->import($callback[0]);
							$arrPagesWithOptions = $this->{$callback[0]}->{$callback[1]}($arrPagesWithOptions, $objRoot->id, true, $objRoot->language);
						}
					}

					if (isset($arrPages) && is_array($arrPages)) 
					{
						foreach ($arrPages as $strUrl) 
						{
							$arrPagesWithOptions[] = array('url' => $strUrl);
						}
					}

					//$objFile = new \File('share/' . $objRoot->sitemapName . '.xml', true);
					$objFile = new \File(\StringUtil::stripRootDir(\System::getContainer()->getParameter('contao.web_dir')) . '/share/' . $objRoot->sitemapName . '.xml');
					$objFile->truncate();
					$objFile->append('<?xml version="1.0" encoding="UTF-8"?>');
					$objFile->append('<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">');
					
					if (!is_array($arrPagesWithOptions)){
						return false;
					}
					
					// Add pages
					foreach ($arrPagesWithOptions as $k)
					{
						$objFile->append('  <url>');

						$objFile->append('    <loc>' . $k['url'] . '</loc>');

						// language relations
						if (isset($k['id']) && in_array('hofff_language_relations', \Config::getActiveModules())) 
						{
							$arrPageLanguageRelations = \ExtendedSitemapLanguageRelations::findPageLanguageRelations($k['id'], $objRoot->language);
						}

						if ($arrPageLanguageRelations != null) 
						{
							foreach ($arrPageLanguageRelations as $relation)
							{
								$objFile->append('    <xhtml:link rel="alternate" hreflang="' . $relation['language'] . '" href="' . $relation['url'] . '" />');
							}
							
							$objFile->append('    <xhtml:link rel="alternate" hreflang="' . $objRoot->language . '" href="' . $k['url'] . '" />');
						}
						
						if (isset($k['lastmod']) && !empty($k['lastmod']))
						{
							$objFile->append('    <lastmod>' . $k['lastmod'] . '</lastmod>');
						}
						
						if (isset($k['changefreq']) && !empty($k['changefreq'])) 
						{
							$objFile->append('    <changefreq>' . $k['changefreq'] . '</changefreq>');
						}
						
						if (isset($k['priority']) && !empty($k['priority'])) 
						{
							$objFile->append('    <priority>' . $k['priority'] . '</priority>');
						}
						
						if (isset($k['ce']) && !empty($k['ce'])) 
						{
							foreach ($k['ce'] as $ce) 
							{
								if ($ce['ceType'] == 'image') 
								{
									$objFile->append('    <image:image>');
									$objFile->append('      <image:loc>' . $ce['ceUrl'] . '</image:loc>');

									if (isset($ce['ceTitle']) && !empty($ce['ceTitle'])) 
									{
										$objFile->append('      <image:title>' . $ce['ceTitle'] . '</image:title>');
									}
									
									if (isset($ce['ceCaption']) && !empty($ce['ceCaption'])) 
									{
										$objFile->append('      <image:caption>' . $ce['ceCaption'] . '</image:caption>');
									}
									
									if (isset($ce['ceImageLocation']) && !empty($ce['ceImageLocation'])) 
									{
										$objFile->append('      <image:geo_location>' . $ce['ceImageLocation'] . '</image:geo_location>');
									}
									
									if (isset($ce['ceImageLicense']) && !empty($ce['ceImageLicense'])) 
									{
										$objFile->append('      <image:license>' . $ce['ceImageLicense'] . '</image:license>');
									}
									
									$objFile->append('    </image:image>');
								}
								elseif ($ce['ceType'] == 'downloads')
								{
									foreach ($ce['ceUrl'] as $k)
									{
										$objFile->append('  </url>');
										$objFile->append('  <url>');
										$objFile->append('    <loc>' . $k . '</loc>');
									}
								}									
								else
								{
									$objFile->append('  </url>');
									$objFile->append('  <url>');
									$objFile->append('    <loc>' . $ce['ceUrl'] . '</loc>');
								}
							}
						}

						if (isset($k['articles']) && !empty($k['articles']))
						{
							foreach ($k['articles'] as $arrArticle)
							{
								$objFile->append('  </url>');
								$objFile->append('  <url>');
								$objFile->append('    <loc>' . $arrArticle['url'] . '</loc>');
								$objFile->append('    <changefreq>' . $arrArticle['changefreq'] . '</changefreq>');
								$objFile->append('    <lastmod>' . $arrArticle['lastmod'] . '</lastmod>');
								$objFile->append('    <priority>' . $arrArticle['priority'] . '</priority>');
							}
						}
												
						$objFile->append('  </url>');
					}

					$objFile->append('</urlset>');
					$objFile->close();

					// Add a log entry
					\System::log('Generated sitemap "' . $objRoot->sitemapName . '.xml" (extendedSitemap)', __METHOD__, TL_CRON);
				}			
			}

			if (\Config::get('activateSitemapIndex') == true)
			{
				\ExtendedSitemap::buildSitemapIndex();

				\System::log('Generated sitemapIndex "' . \Config::get('sitemapIndexName') . '.xml" (extendedSitemap)', __METHOD__, TL_CRON);
			}

			/* Leave this out for now...
			if (\Config::get('sitemapPingGoogleBing') == true)
			{
				if ($objRoot->inSitemapIndex == true)
				{
					\ExtendedSitemap::pingGoogleBing($objRoot->id);
				}
			}
			*/
		}
	}
}

?>