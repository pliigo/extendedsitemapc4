<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2017 Leo Feyer
 *
 * @package sd_extendedSitemap
 *
 * @copyright SmithData / Patrick Smith 2017
 *
 * @license GPLv3+
 *
 * @author Patrick Smith <https://smithdata.de>
 * 
 * Based on googleSitemap from Andreas Schempp <https://terminal42.ch>
 */

namespace smithdata\extendedSitemap;

/**
 * Class extendedSitemap
 */
class ExtendedSitemap extends \Backend
{
	/**
	 * Only root pages should have sitemap names
	 */
	public function deleteSitemapNamesForNonRootPages()
	{
		$objDatabase = \Database::getInstance();

		$objDatabase->prepare("UPDATE tl_page SET createSitemap = '', sitemapName = '' WHERE type != 'root'")->execute();
	}


	/**
	 * Get all root pages
	 *
	 * @param array $arrOptions
	 *
	 * @return object
	 */
	public function findPublishedRootPages($arrOptions)
	{
		return \PageModel::findPublishedRootPages($arrOptions);
	}


	/**
	 * Get all searchable pages
	 *
	 * @param array $arrOptions
	 *
	 * @return object
	 */
	public static function findSearchablePages($pid=0, $domain='', $blnIsSitemap=false)
	{
		return parent::findSearchablePages($pid, $domain, $blnIsSitemap);
	}


	/**
	 * Get all searchable pages with options and files/images
	 *
	 * @param integer $pid
	 * @param boolean $blnIsSitemap
	 * @param string $ptable
	 * @param boolean $blnNewCycle
	 *
	 * @return array
	 */
	public static function findSearchablePagesWithOptions($pid=0, $blnIsSitemap=false, $rootLanguage=null)
	{
		$time = time();

		$objPages = \PageModel::findBy(array('pid=?'), array($pid), array(array('order'), array('sorting')));

		$objRegistry = \Model\Registry::getInstance();

		if($objPages != null)
		{
			while($objPages->next())
			{
				$objPage = $objRegistry->fetch('tl_page', $objPages->id);

				if ($objPage === null)
				{
					$objPage = new \PageModel($objPages);
				}

				if ($objPage->type == 'regular')
				{
					// Searchable and not protected
					if ((!$objPage->noSearch || $blnIsSitemap) && (!$objPage->protected || \Config::get('indexProtected') && (!$blnIsSitemap || $objPage->sitemap == 'map_always')) && (!$blnIsSitemap || $objPage->sitemap != 'map_never'))
					{
						// Published
						if ($objPage->published && ($objPage->start == '' || $objPage->start <= $time) && ($objPage->stop == '' || $objPage->stop > ($time + 60)))
						{
							$arrPage = array();

							$strPageUrl = $objPage->getAbsoluteUrl();
							$strPageUrl = rawurlencode($strPageUrl);
							$strPageUrl = str_replace(array('%2F', '%3F', '%3D', '%26', '%3A//'), array('/', '?', '=', '&', '://'), $strPageUrl);
							$strPageUrl = ampersand($strPageUrl, true);

							$arrPage['id'] = $objPage->id;
							$arrPage['url'] = $strPageUrl;
							$arrPage['lastmod'] = $objPage->sitemapLastmodDate != '' ? date("Y-m-d", $objPage->sitemapLastmodDate) : date("Y-m-d", time());
							$arrPage['changefreq'] = $objPage->sitemapChangefreq;
							$arrPage['priority'] = $objPage->sitemapPriority != 0 ?  '0.' . $objPage->sitemapPriority : '1.0';

							$objArticles = \ArticleModel::findBy(array('pid=?','published=?'), array($objPage->id, '1'));

							if($objArticles != null)
							{
								while($objArticles->next())
								{
									if (($objArticles->start == '' || $objArticles->start <= $time) && ($objArticles->stop == '' || $objArticles->stop >= $time))
									{
										if ($objArticles->showTeaser == 1) 
										{
											$strAliasOrId = ($objArticles->alias != null && !\Config::get('disableAlias')) ? $objArticles->alias : $objArticles->id;

											$arrArticles['id'] = $objPage->id . '.' . $objArticles->id;
											$arrArticles['url'] = $strPageUrl . '/articles/' . $strAliasOrId;
											$arrArticles['lastmod'] = $objPage->sitemapLastmodDate != '' ? date("Y-m-d", $objPage->sitemapLastmodDate) : date("Y-m-d", time());
											$arrArticles['changefreq'] = $objPage->sitemapChangefreq;
											$arrArticles['priority'] = $objPage->sitemapPriority != 0 ?  '0.' . $objPage->sitemapPriority : '1.0';

											$arrPage['articles'][] = $arrArticles;
										}
										
										$objContentElements = \ContentModel::findBy(array('pid=?', 'ptable=?', 'addToSitemap=?', 'invisible!=?', '((type="image" OR type="download" OR type="downloads" OR type="gallery") OR (type="text" AND addImage="1"))'), array($objArticles->id, 'tl_article', '1', '1'));

										if($objContentElements != null)
										{
											while($objContentElements->next())
											{
												if ($objContentElements->type == 'downloads' || $objContentElements->type == 'gallery')
												{
													if (isset($objContentElements->multiSRC))
													{
														$objCeFiles = \FilesModel::findMultipleByUuid(deserialize($objContentElements->multiSRC));

														if ($objCeFiles != null)
														{
															while ($objCeFiles->next())
															{
																$arrCe = array();

																$arrCe['ceId'] = $objPage->id . '.' . $objArticles->id . '.' . $objContentElements->id . '.' . $objCeFiles->id;
																$arrCe['ceType'] = $objContentElements->type == 'gallery' ? 'image' : $objContentElements->type;
																$arrCe['ceUrl'] = \Environment::get('base') . $objCeFiles->path;
																
																if ($objCeFiles->meta != null)
																{
																	$arrMeta = deserialize($objCeFiles->meta);

																	if (isset($arrMeta[$rootLanguage]))
																	{
																		$arrCe['ceTitle'] = $arrMeta[$rootLanguage]['title'];
																		$arrCe['ceCaption'] = $arrMeta[$rootLanguage]['caption'];
																	}
																}
																
																$arrPage['ce'][] = $arrCe;
															}
														}
													}
												}
												else
												{
													$arrCe = array();

													$objCeFile = \FilesModel::findByUuid($objContentElements->singleSRC);

													$arrCe['ceId'] = $objContentElements->id;
													$arrCe['ceType'] = $objContentElements->type == 'text' ? 'image' : $objContentElements->type;
													$arrCe['ceUrl'] = \Environment::get('base') . $objCeFile->path;
													
													if ($objCeFile->meta != null)
													{
														$arrMeta = deserialize($objCeFile->meta);

														if (isset($arrMeta[$rootLanguage]))
														{
															$arrCe['ceTitle'] = $arrMeta[$rootLanguage]['title'];
															$arrCe['ceCaption'] = $arrMeta[$rootLanguage]['caption'];
														}
													}
													else
													{
														if (isset($objContentElements->title))
														{
															$arrCe['ceTitle'] = $objContentElements->title;
														}
														
														if (isset($objContentElements->caption))
														{
															$arrCe['ceCaption'] = $objContentElements->caption;
														}
														
													}

													if ($objContentElements->type == 'image' || $objContentElements->type == 'text')
													{
														$arrCe['ceImageLocation'] = $objContentElements->sitemapImageLocation;
														$arrCe['ceImageLicense'] = $objContentElements->sitemapImageLicense;
													}
													
													$arrPage['ce'][] = $arrCe;
												}
											}
										}
									}
								}
							}

							$arrReturn[] = $arrPage;
						}
					}	
				}
				
				// Get subpages
				if ((!$objPage->protected || \Config::get('indexProtected')) && ($arrSubpages = static::findSearchablePagesWithOptions($objPage->id, $blnIsSitemap, $rootLanguage)) != false)
				{
					if (! is_array($arrReturn)){
						$arrReturn = [];
					}
					$arrReturn = array_merge($arrReturn, $arrSubpages);
				}
			}
		}

		return $arrReturn;
	}


	/**
	 * Ping google and bing
	 *
	 * @param int $rootId
	 */
	public function pingGoogleBing($rootId, $inSitemapIndex=false)
	{
		$objRoot = \PageModel::findBy(array('id=?'), array($rootId));

		if ($objRoot != null && $objRoot->type != 'regular')
		{
			if (\Config::get('sitemapPingGoogleBing') && $objRoot->sitemapLastPing < (time() - 3600000) && $objRoot->inSitemapIndex == false)
			{
				$objPing = new \Request();
				
				//$pingGoogle = $objPing->send('http://www.google.com/ping?sitemap=' . $sitemapName . '.xml');
				//$pingBing = $objPing->send('http://www.bing.com/ping?sitemap=' . $sitemapName . '.xml');

				unset($objPing);

				$objRoot->sitemapLastPing = time();
				$objRoot->save();
			}
		}

		
	}


	/**
	 * Automatically update the lastmod for the page from page, articles or content elements
	 *
	 * @param DataContainer $dc
	 */
	public function updateSitemapLastmod(\Contao\DataContainer $dc)
	{
		$time = time();

		$objDatabase = \Database::getInstance();

		if ($dc->table == 'tl_content')
		{
			$objContentElement = $objDatabase->prepare("SELECT * FROM tl_content WHERE id=?")->limit(1)->execute($dc->id);
		}

		if ($dc->table != 'tl_page')
		{
			$objArticle = $objDatabase->prepare("SELECT * FROM tl_article WHERE id=?")->limit(1)->execute($objContentElement != null ? $objContentElement->pid : $dc->id);

			$objPage = $objDatabase->prepare("SELECT * FROM tl_page WHERE id=?")->limit(1)->execute($objArticle->pid);
		}

		$objLastmodDate = $objDatabase->prepare('UPDATE tl_page SET sitemapLastmodDate=? WHERE id=?')->execute($time, $objPage != null ? $objPage->id : $dc->id);
	}


	/**
	 * Build the sitemap index file
	 *
	 * @param int $rootId
	 */
	public function buildSitemapIndex()
	{
		$objDatabase = \Database::getInstance();

		$objSitemaps = $objDatabase->prepare("SELECT * FROM tl_page WHERE inSitemapIndex =? AND sitemapName !=?")->execute('1','');

		if ($objSitemaps != null)
		{
			//$objFile = new \File('share/' . \Config::get('sitemapIndexName') . '.xml', true);
			$objFile = new \File(\StringUtil::stripRootDir(\System::getContainer()->getParameter('contao.web_dir')) . '/share/' . \Config::get('sitemapIndexName') . '.xml');
			$objFile->truncate();
			$objFile->append('<?xml version="1.0" encoding="UTF-8"?>');
			$objFile->append('<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">');
			
			while ($objSitemaps->next())
			{
				$objFile->append('  <sitemap>');
				$objFile->append('    <loc>' . \Environment::get('base') . 'share/' . $objSitemaps->sitemapName . '.xml</loc>');
				$objFile->append('    <lastmod>' . date('Y-m-d', time()) . '</lastmod>');
				$objFile->append('  </sitemap>');
			}

			$objFile->append('</sitemapindex>');
			$objFile->close();
		}
	}


	/**
	 * Get the list with image licenses
	 *
	 * @return array
	 */
	public function getImageLicenses()
	{
		return array
		(
			'https://directory.fsf.org/wiki/License:Apache2.0' => 'Apache License 2.0',
			'https://creativecommons.org/licenses/by/4.0' => 'Creative Commons BY',
			'https://creativecommons.org/licenses/by-sa/4.0' => 'Creative Commons BY-SA',
			'https://creativecommons.org/licenses/by-nd/4.0' => 'Creative Commons BY-ND',
			'https://creativecommons.org/licenses/by-nc/4.0' => 'Creative Commons BY-NC',
			'https://creativecommons.org/licenses/by-nc-sa/4.0' => 'Creative Commons BY-NC-SA',
			'https://creativecommons.org/licenses/by-nc-nd/4.0' => 'Creative Commons BY-NC-ND',
			'https://www.gnu.org/licenses/gpl' => 'GNU GPL v3',
			'https://www.gnu.org/licenses/old-licenses/gpl-2.0' => 'GNU GPL v2',
			'https://www.gnu.org/licenses/lgpl' => 'GNU LGPL v3',
			'https://www.gnu.org/licenses/old-licenses/lgpl-2.1' => 'GNU LGPL v2.1',
			'https://www.gnu.org/licenses/agpl-3.0' => 'GNU AGPL v3'
		);
	}
}